import {Component} from '@angular/core';

@Component({
  selector: 'app-generations',
  templateUrl: './generations.component.html',
  styleUrls: ['./generations.component.scss']
})
export class GenerationsComponent {

  public generations: string[];

  constructor() {
    this.generations = [
      "Generation I",
      "Generation II",
      "Generation III",
      "Generation IV",
      "Generation V",
      "Generation VI",
    ]
  }
}
