import {Component, Inject} from '@angular/core';
import {Pokemon} from "../../modeles/Pokemon";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss']
})
export class PokemonsComponent {
  public pokemons: Observable<Pokemon>[];

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    this.pokemons = [];
    for (let i = 1; i < 1000; i++) {
      let pokemon$ = this.http.get<Pokemon>('https://pokeapi.co/api/v2/pokemon/'+i);
      this.pokemons.push(pokemon$);
    }
  }
}
